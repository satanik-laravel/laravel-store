<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 10.04.18
 * Time: 09:21
 */

namespace Satanik\Store\Facades;

use Illuminate\Support\Facades\Facade;

class Store extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected static function getFacadeAccessor()
    {
        return 'satanik-store';
    }

}
