<?php

namespace Satanik\Store;

use Assert;
use Satanik\Store\Contracts\Identifyable;
use Satanik\Store\Contracts\Store as StoreContract;

class Store implements StoreContract
{
    /**
     * @param string      $class
     * @param mixed|array $identifier
     *
     * @return mixed|Identifyable|null
     * @throws \Satanik\Exceptions\Types\Exception
     */
    public function identify(string $class, $identifier): ?Identifyable
    {
        Assert::classExists($class);
        Assert::implements($class, Identifyable::class);

        /** @var $class Identifyable */

        return $class::identify($identifier);
    }
}
