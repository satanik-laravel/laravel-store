<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 10.04.18
 * Time: 09:20
 */

namespace Satanik\Store\Contracts;


interface Identifyable
{
    /**
     * @param mixed|array $identifier
     *
     * @return mixed|Identifyable|null
     * @throws \Satanik\Exceptions\Types\Exception
     */
    public static function identify($identifier): ?Identifyable;
}
