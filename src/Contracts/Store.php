<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 10.04.18
 * Time: 09:21
 */

namespace Satanik\Store\Contracts;

interface Store
{
    /**
     * @param string      $class
     * @param mixed|array $identifier
     *
     * @return Identifyable|null
     */
    public function identify(string $class, $identifier): ?Identifyable;
}
