<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 11.04.18
 * Time: 10:54
 */

namespace Satanik\Store\Concerns;

use Assert;
use Satanik\Store\Contracts\Identifyable;
use Satanik\Exceptions\Types\Exception;

trait IdentifyableEloquent
{
    /**
     * @param mixed|array $identifier
     *
     * @return mixed|\Satanik\Store\Contracts\Identifyable|null
     * @throws \Satanik\Exceptions\Types\Exception
     */
    public static function identify($identifier): ?Identifyable
    {
        Assert::is($identifier, 'array', '$identifier');

        if (empty($identifier)) {
            return null;
        }

        if (!array_all($identifier, function ($k, $v) {
            return \is_string($k);
        })) {
            throw new Exception('var_is_not_a', [
                'variable' => '$identifier',
                'type'     => 'associative string-key array',
            ]);
        }
        /** @var \Illuminate\Database\Eloquent\Builder $query */
        $query = static::query();

        foreach ($identifier as $key => $value) {
            $query->where($key, $value);
        }

        if ($query->count() == 1) {
            return $query->first();
        }

        return null;
    }
}
