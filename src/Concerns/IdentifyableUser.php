<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 11.04.18
 * Time: 11:04
 */

namespace Satanik\Store\Concerns;

use Satanik\Exceptions\Types\Exception;
use Satanik\Store\Contracts\Identifyable;

trait IdentifyableUser
{
    use IdentifyableEloquent, IdentifyableAuthenticatable {
        IdentifyableEloquent::identify as protected eloquent_identify;
        IdentifyableAuthenticatable::identify as protected auth_identify;
    }

    protected static $identifier    = 'id';
    protected static $unique_fields = ['email'];

    /**
     * @param mixed|array $identifier
     *
     * @return mixed|\Satanik\Store\Contracts\Identifyable|null
     * @throws \Satanik\Exceptions\Types\Exception
     */
    public static function identify($identifier): ?Identifyable
    {
        if (!\is_array($identifier)) {
            if ($user = static::auth_identify($identifier)) {
                return $user;
            }

            if ($user = static::eloquent_identify([static::$identifier => $identifier])) {
                return $user;
            }

            foreach (static::$unique_fields as $field) {
                if ($user = static::eloquent_identify([$field => $identifier])) {
                    return $user;
                }
            }

            return null;
        }

        if (!array_all($identifier, function ($k, $v) {
            return \is_string($k);
        })) {
            throw new Exception('var_is_not_a', [
                'variable' => '$identifier',
                'type'     => 'associative string-key array',
            ]);
        }

        $model = static::eloquent_identify(array_except($identifier, 'token'));

        if (isset($identifier['token'])) {
            $authenticatable = static::auth_identify($identifier['token']);
        }

        if (empty($authenticatable)) {
            return $model;
        }

        if (empty($model)) {
            return $authenticatable;
        }

        return $authenticatable == $model ? $model : null;
    }
}
