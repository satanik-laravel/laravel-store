<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 11.04.18
 * Time: 11:00
 */

namespace Satanik\Store\Concerns;


use Satanik\Store\Contracts\Identifyable;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Token;

trait IdentifyableAuthenticatable
{
    /**
     * @param mixed|array $identifier
     *
     * @return mixed|\Satanik\Store\Contracts\Identifyable|null
     */
    public static function identify($identifier): ?Identifyable
    {
        while ($identifier instanceof Token) {
            $identifier = $identifier->get();
        }

        if (\is_array($identifier) && isset($identifier['token'])) {
            $identifier = $identifier['token'];
        }

        try {
            \JWTAuth::setToken($identifier);
            if ($user =  \JWTAuth::toUser()) {
                return $user;
            }
        } catch (JWTException $e) {
            return null;
        } finally {
            \JWTAuth::unsetToken();
        }
        return null;
    }
}
