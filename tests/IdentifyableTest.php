<?php

/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 26.03.18
 * Time: 12:46
 */

namespace Satanik\Foundation\Tests;

use Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\MockObject\MockObject;
use Satanik\Store\Concerns\IdentifyableAuthenticatable;
use Store;
use Tests\TestCase;
use Tymon\JWTAuth\Token;

class IdentifyableTest extends TestCase
{
    use RefreshDatabase;

//    private static $seeded = false;
//    protected      $user;
//
//    /**
//     * Setup the test environment.
//     *
//     * @return void
//     * @throws \Exception
//     * @throws \Throwable
//     */
//    protected function setUp(): void
//    {
//        parent::setUp();
//
//        if (!self::$seeded) {
//            Artisan::call('db:seed');
//            self::$seeded = true;
//        }
//
//        $this->user = factory(\App\User::class)->create();
//    }

    /**
     * @throws \ReflectionException
     */
    public function testIdentifyableAuthenticatable(): void
    {
        /** @var IdentifyableAuthenticatable|MockObject $mock */
        $mock = $this->getMockForTrait('Satanik\Store\Concerns\IdentifyableAuthenticatable');

        $this->assertEquals(
            invoke_method($mock, 'identify', [$this->user->token])->id,
            $this->user->id
        );
        $this->assertEquals(
            invoke_method($mock, 'identify', array(['token' => $this->user->token]))->id,
            $this->user->id
        );
        $this->assertEquals(
            invoke_method($mock, 'identify', array(new Token($this->user->token)))->id,
            $this->user->id
        );
    }

    public function testIdentifyableEloquent(): void
    {
        $this->assertEquals(
            Store::identify(\App\User::class, $this->user->id)->id,
            $this->user->id
        );
        $this->assertEquals(
            Store::identify(\App\User::class, $this->user->email)->id,
            $this->user->id
        );

        $this->assertEquals(
            Store::identify(\App\User::class, ['id' => $this->user->id])->id,
            $this->user->id
        );
        $this->assertEquals(
            Store::identify(\App\User::class, ['email' => $this->user->email])->id,
            $this->user->id
        );
    }

    public function testIdentifyUserWithTokenAndDataSucceeds(): void
    {
        $this->assertEquals(
            Store::identify(\App\User::class, [
                'id'    => $this->user->id,
                'email' => $this->user->email,
                'token' => $this->user->token,
            ])->id,
            $this->user->id
        );
        $this->assertEquals(
            Store::identify(\App\User::class, $this->user->token)->id,
            $this->user->id
        );
        $this->assertEquals(
            Store::identify(\App\User::class, ['id' => $this->user->id])->id,
            $this->user->id
        );
    }

    public function testIdentifyUserWithTokenStringSucceeds(): void
    {
        $this->assertEquals(
            Store::identify(\App\User::class, $this->user->token)->id,
            $this->user->id
        );
    }

    public function testIdentifyUserWithTokenArraySucceeds(): void
    {
        $this->assertEquals(
            Store::identify(\App\User::class, ['token' => $this->user->token])->id,
            $this->user->id
        );
    }

    public function testIdentifyUserWithTokenObjectSucceeds(): void
    {
        $this->assertEquals(
            Store::identify(\App\User::class, new Token($this->user->token))->id,
            $this->user->id
        );
    }

    public function testIdentifyUserWithIdSucceeds(): void
    {
        $this->assertEquals(
            Store::identify(\App\User::class, ['id' => $this->user->id])->id,
            $this->user->id
        );
    }

    public function testIdentifyableUserWithContradictingDataAndTokenFails(): void
    {
        $user = factory(\App\User::class)->create();

        $this->assertNull(
            Store::identify(\App\User::class, [
                'id'    => $this->user->id,
                'email' => $this->user->email,
                'token' => \JWTAuth::fromUser($user),
            ])
        );
    }

    public function testIdentifyableUserWithContradictingDataFails(): void
    {
        $user = factory(\App\User::class)->create();

        $this->assertNull(
            Store::identify(\App\User::class, [
                'id'    => $this->user->id,
                'email' => $user->email,
            ])
        );
    }

    public function testIdentifyableUserWithAmbiguousResultFails(): void
    {
        factory(\App\User::class)->create([
            'name' => $this->user->firstname,
        ]);

        $this->assertNull(
            Store::identify(\App\User::class, [
                'name' => $this->user->firstname,
            ])
        );
    }

    public function testIdentifyableUserWithoutDataFails(): void
    {
        $this->assertNull(
            Store::identify(\App\User::class, [])
        );
    }
}
