# Satanik Store Package

[![Software License][ico-license]](LICENSE.md)

This package adds the functionality of an object store to specific class. Thus finding inidiviual objects is greatly improved and accelerated.

### Dependencies

* satanik/exceptions - [git@gitlab.com:satanik/laravel-exceptions.git]()

## Structure

The directory structure follows the industry standard.

```bash
config/
src/
tests/
```


## Install

Via Composer

``` bash
$ composer require satanik/store
```

if the package is not published use this in the composer file

```json
"repositories": {
  "satanik/store": {
    "type": "vcs",
    "url": "git@gitlab.com:satanik/laravel-store.git"
  },
  ...
},
...
"require": {
  ...
  "satanik/store": "<version constraint>",
```

or copy the repository to

```bash
<project root>/packages/Satanik/Store
```

and use this code in the composer file

```json
"repositories": {
  "satanik/store": {
    "type": "path",
    "url": "packages/Satanik/Store",
    "options": {
      "symlink": true
    }
  },
  ...
},
...
"require": {
  ...
  "satanik/store": "@dev",
```

## Usage

### Store

The `Store`  facade provides just one simple function to find objects.

```php
use Satanik\Store\Contracts\Identifyable;

/**
  * @param string      $class
  * @param mixed|array $identifier
  *
  * @return Identifyable|null
  */
\Store::identify(string $class, $identifier): ?Identifyable;
```

Thus, a class needs to implement the `Satanik\Store\Contracts\Identifyable` interface. This also adds the same `identify` method to the class as well, not needing the detour of using the facade.

### Concerns

To implement the `Satanik\Store\Contracts\Identifyable` interface the package provides three different traits.

```php
\Satanik\Store\Concerns\IdentifyableAuthenticatable; // for JWT authenticatables
\Satanik\Store\Concerns\IdentifyableEloquent; // for models
\Satanik\Store\Concerns\IdentifyableUser; // for users using both JWT and being models
```


## Testing

``` bash
$ composer test
```

## Security

If you discover any security related issues, please email daniel@satanik.at instead of using the issue tracker.

## Credits

- [Daniel Satanik][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square

[link-author]: https://satanik.at
